import { Component, OnInit } from '@angular/core';

import * as NEWS_CONSTANTS from './news.constants';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  
  constants = NEWS_CONSTANTS;

  constructor() { }

  ngOnInit() {
  }

}
