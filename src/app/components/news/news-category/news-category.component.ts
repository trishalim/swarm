import { Component, OnInit, Input } from '@angular/core';

import { CATEGORIES } from '../news.constants';

@Component({
  selector: 'app-news-category',
  templateUrl: './news-category.component.html',
  styleUrls: ['./news-category.component.scss']
})
export class NewsCategoryComponent implements OnInit {
  
  @Input() category: string;

  constructor() { }

  ngOnInit() {  
  }
  
  getCategoryColor() {
    switch (this.category) {
      case (CATEGORIES.team): return 'yellow';
      case (CATEGORIES.injuries): return 'violet';
      case (CATEGORIES.playoffs): return 'orange';
    }
  }

}
