import { Component, OnInit, Input } from '@angular/core';

import * as NEWS_CONSTANTS from '../news.constants';

@Component({
  selector: 'app-news-article',
  templateUrl: './news-article.component.html',
  styleUrls: ['./news-article.component.scss']
})
export class NewsArticleComponent implements OnInit {

  constants = NEWS_CONSTANTS;

  // post content
  @Input() title: string;
  @Input() author: string;
  @Input() date: string;
  @Input() category: string;
  @Input() description = 'Lorem ipsum dolor sit amet, consectetur adipisi nel elit, '
   + 'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. '
   + 'Ut enim ad mini veniam, quis nostrud en derum sum laborem.';

  // post stats
  @Input() views: number;
  @Input() likes: number;
  @Input() comments: number;
  @Input() liked: boolean;

  // determines how the news article is displayed
  @Input() type = NEWS_CONSTANTS.DISPLAY_TYPES.vertical;

  categoryColor: string;

  constructor() { }

  ngOnInit() {
    switch (this.category) {
      case (NEWS_CONSTANTS.CATEGORIES.team):
        this.categoryColor = 'yellow';
        break;
      case (NEWS_CONSTANTS.CATEGORIES.injuries):
        this.categoryColor = 'violet';
        break;
      case (NEWS_CONSTANTS.CATEGORIES.playoffs):
        this.categoryColor = 'orange';
        break;
    }
  }

}
