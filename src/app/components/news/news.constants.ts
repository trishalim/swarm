// categories for news articles
export const CATEGORIES = {
  team: 'The Team',
  injuries: 'Injuries',
  playoffs: 'Playoffs',
};

// different ways of how an article is displayed
export const DISPLAY_TYPES = {
  vertical: 'vertical',
  short_vertical: 'short_vertical',
  horizontal: 'horizontal',
  small: 'small'
};
