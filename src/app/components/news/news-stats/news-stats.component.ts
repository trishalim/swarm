import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-news-stats',
  templateUrl: './news-stats.component.html',
  styleUrls: ['./news-stats.component.scss']
})
export class NewsStatsComponent implements OnInit {
  
  @Input() views: number;
  @Input() likes: number;
  @Input() comments: number;
  @Input() liked: boolean;

  constructor() { }

  ngOnInit() {
  }

}
