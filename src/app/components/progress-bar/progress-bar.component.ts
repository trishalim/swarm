import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.scss']
})
export class ProgressBarComponent implements OnInit {

  @Input() leftScore: number;
  @Input() rightScore: number;
  leftWidth: number;
  rightWidth: number;

  constructor() { }

  ngOnInit() {
    // get percentage of score
    // assuming 40 is the highest score
    // percentage should be rounded off to tens place
    this.leftWidth = Math.ceil( (this.leftScore / 40 * 100) / 10) * 10;
    this.rightWidth = Math.ceil( (this.rightScore / 40 * 100) / 10) * 10;
  }

}
