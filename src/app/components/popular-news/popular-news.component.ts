import { Component, OnInit } from '@angular/core';

import * as NEWS_CONSTANTS from '../news/news.constants';

@Component({
  selector: 'app-popular-news',
  templateUrl: './popular-news.component.html',
  styleUrls: ['./popular-news.component.scss']
})
export class PopularNewsComponent implements OnInit {
  
  constants = NEWS_CONSTANTS;

  constructor() { }

  ngOnInit() {
  }

}
