import { Component, OnInit } from '@angular/core';
import 'bootstrap';

import * as NEWS_CONSTANTS from '../news/news.constants';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {

  constants = NEWS_CONSTANTS;

  carouselItems = [
    {
      title: 'All the players are taking a team trip this summer',
      date: 'November 8th, 2015',
      category: 'CATEGORIES.team',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisi nel elit, '
       + 'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
      views: 2369,
      likes: 530,
      comments: 18,
    },
    {
      title: 'Swarm women team tryouts will start in January',
      date: 'November 8th, 2015',
      category: 'CATEGORIES.team',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisi nel elit, '
       + 'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
      views: 2369,
      likes: 530,
      comments: 18,
    },
    {
      title: 'Check out the new ride of our best player of the season',
      date: 'November 8th, 2015',
      category: 'CATEGORIES.team',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisi nel elit, '
       + 'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
      views: 2369,
      likes: 530,
      comments: 18,
    },
  ]

  constructor() { }

  ngOnInit() {
  }

}
