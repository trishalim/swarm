import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

// Header
import { HeaderComponent } from './components/header/header.component';
import { MainMenuComponent } from './components/main-menu/main-menu.component';
import { MiddleMenuComponent } from './components/middle-menu/middle-menu.component';
import { SliderComponent } from './components/slider/slider.component';

// Main content
import { MainComponent } from './components/main/main.component';
import { NewsComponent } from './components/news/news.component';
import { NewsArticleComponent } from './components/news/news-article/news-article.component';
import { NewsStatsComponent } from './components/news/news-stats/news-stats.component';
import { NewsCategoryComponent } from './components/news/news-category/news-category.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';

// Footer
import { FooterComponent } from './components/footer/footer.component';
import { InstagramWidgetComponent } from './components/instagram-widget/instagram-widget.component';
import { PopularNewsComponent } from './components/popular-news/popular-news.component';
import { ContactInfoComponent } from './components/contact-info/contact-info.component';
import { ProgressBarComponent } from './components/progress-bar/progress-bar.component';

@NgModule({
  declarations: [
    AppComponent,

    // Header
    HeaderComponent,
    MainMenuComponent,
    MiddleMenuComponent,
    SliderComponent,

    // Main content
    MainComponent,
    NewsComponent,
    NewsArticleComponent,
    NewsStatsComponent,
    NewsCategoryComponent,
    SidebarComponent,

    // Footer
    FooterComponent,
    InstagramWidgetComponent,
    PopularNewsComponent,
    ContactInfoComponent,
    ProgressBarComponent,
  ],
  imports: [
    BrowserModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
